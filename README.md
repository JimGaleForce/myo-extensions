# README #

myo-extensions are lua specific classes to make coding for the myo easier, using the myo manager

### What is this repository for? ###

* Anyone wanting a quick way to make the myo do what you want for a program.
* Programmers wanting to use a simple object oriented base for creating custom apps.

### How do I get set up? ###

* Get a myo armband.
* Get this repository.
* Check out the vlc.lua as a sample (requires the myocontroller or base). 

### Who do I talk to? ###

* write source@jimgale.net for further information